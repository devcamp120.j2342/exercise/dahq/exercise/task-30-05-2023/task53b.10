import model.Author;
import model.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author();
        author1.setName("Nguyen Van A");
        author1.setEmail("a@gmail.com");
        author1.setGender('m');
        Author author2 = new Author("Nguyen Van B", "b@gmail.com", 'f');
        System.out.println(author1.toString());
        System.out.println(author2.toString());
        Book book1 = new Book("Chiec la cuoi cung", 90.8, 10, author2);
        Book book2 = new Book("Mat Biec", 89.8, author1);
        System.out.println(book1.toString());
        System.out.println(book2.toString());

    }
}
